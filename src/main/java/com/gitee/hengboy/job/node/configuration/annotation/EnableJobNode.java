package com.gitee.hengboy.job.node.configuration.annotation;

import com.gitee.hengboy.job.node.configuration.JobNodeConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * enable job node annotation
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2019-01-07
 * Time：14:47
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({JobNodeConfiguration.class})
public @interface EnableJobNode {
}
