package com.gitee.hengboy.job.node.configuration;

import com.gitee.hengboy.distributed.job.configuration.JobBasicConfiguration;
import com.gitee.hengboy.job.node.configuration.properties.JobNodeProperties;
import com.weibo.api.motan.config.springsupport.BasicRefererConfigBean;
import com.weibo.api.motan.config.springsupport.RegistryConfigBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * node自动化配置类
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2019-01-05
 * Time：16:01
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@Configuration
@EnableConfigurationProperties(JobNodeProperties.class)
@Import({JobBasicConfiguration.class, JobNodeCommandLineRunnerConfiguration.class})
public class JobNodeConfiguration {
    /**
     * 自定义配置属性类
     */
    private final JobNodeProperties jobNodeProperties;

    /**
     * 构造函数注入配置属性类
     *
     * @param jobNodeProperties 配置属性类
     */
    public JobNodeConfiguration(JobNodeProperties jobNodeProperties) {
        this.jobNodeProperties = jobNodeProperties;
    }

    /**
     * 配置motan客户端
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public BasicRefererConfigBean basicRefererConfig() {
        BasicRefererConfigBean config = new BasicRefererConfigBean();
        config.setProtocol(jobNodeProperties.getProtocol());
        config.setApplication("jobApplication");
        config.setGroup(jobNodeProperties.getGroup());
        config.setCheck(jobNodeProperties.getCheck());
        config.setRequestTimeout(jobNodeProperties.getRequestTimeout());
        config.setRegistry(jobNodeProperties.getRegistryName());
        config.setRetries(jobNodeProperties.getRetries());
        config.setThrowException(jobNodeProperties.getThrowException());
        return config;
    }


    /**
     * job 注册中心配置
     *
     * @return
     */
    @Bean("jobRegistry")
    @ConditionalOnMissingBean
    public RegistryConfigBean registryConfigBean() {
        RegistryConfigBean registryConfigBean = new RegistryConfigBean();
        registryConfigBean.setRegProtocol(jobNodeProperties.getRegProtocol());
        registryConfigBean.setAddress(jobNodeProperties.getRegAddress() + ":" + jobNodeProperties.getRegPort());
        return registryConfigBean;
    }
}
