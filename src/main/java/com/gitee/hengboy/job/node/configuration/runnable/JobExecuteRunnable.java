package com.gitee.hengboy.job.node.configuration.runnable;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * 任务执行线程
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2019-01-08
 * Time：17:34
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@AllArgsConstructor
@NoArgsConstructor
public class JobExecuteRunnable implements Runnable {

    @Override
    public void run() {

    }
}
