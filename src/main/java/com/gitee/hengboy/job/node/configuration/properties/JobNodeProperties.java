package com.gitee.hengboy.job.node.configuration.properties;

import com.weibo.api.motan.config.springsupport.BasicRefererConfigBean;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.gitee.hengboy.job.node.configuration.properties.JobNodeProperties.JOB_NODE_PREFIX;

/**
 * job client config properties
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2019-01-05
 * Time：16:05
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@ConfigurationProperties(prefix = JOB_NODE_PREFIX)
@Data
public class JobNodeProperties extends BasicRefererConfigBean {
    /**
     * config properties prefix
     */
    public static final String JOB_NODE_PREFIX = "hengboy.job.node";
    /**
     * register protocol
     */
    private String protocol = "job";
    /**
     * group name
     */
    private String group = "jobGroup";
    /**
     * registry name
     */
    private String registryName = "jobRegistry";
    /**
     * set
     */
    private boolean throwException = true;
    /**
     * set retry times
     */
    private Integer retries = 2;
    /**
     * registry protocol
     * direct is local protocol consumer
     */
    private String regProtocol = "direct";
    /**
     * registry port
     */
    private Integer regPort = 9999;
    /**
     * registry address
     */
    private String regAddress = "127.0.0.1";

}
