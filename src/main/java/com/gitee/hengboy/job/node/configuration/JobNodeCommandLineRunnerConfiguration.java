package com.gitee.hengboy.job.node.configuration;

import com.gitee.hengboy.distributed.job.common.service.HeartSyncService;
import com.gitee.hengboy.distributed.job.thread.JobThread;
import com.gitee.hengboy.job.node.configuration.runnable.HeartSyncRunnable;
import com.gitee.hengboy.job.node.configuration.runnable.JobExecuteRunnable;
import com.weibo.api.motan.config.springsupport.annotation.MotanReferer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * job node command line runner
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2019-01-08
 * Time：15:30
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@Configuration
public class JobNodeCommandLineRunnerConfiguration {

    /**
     * job node startup execute
     * - 开启心跳检查线程
     * - 开启执行任务检查线程
     *
     * @return
     */
    @Bean(name = "jobCommandLineRunner")
    public CommandLineRunner commandLineRunner() {
        return new JobCommandLineRunner();
    }

    /**
     * job command line runner
     */
    class JobCommandLineRunner implements CommandLineRunner {
        /**
         * 心跳检查RPC Service
         */
        @MotanReferer(basicReferer = "basicRefererConfig")
        private HeartSyncService heartSyncService;

        @Override
        public void run(String... args) throws Exception {
            // 开启心跳检查,每隔5秒执行同步一次
            JobThread.scheduleWithFixedDelay(new HeartSyncRunnable(heartSyncService), 5);

            // 开启任务执行检查，每隔2秒执行任务读取
            JobThread.scheduleWithFixedDelay(new JobExecuteRunnable(), 2);
        }
    }
}
