package com.gitee.hengboy.job.node.configuration.runnable;

import com.gitee.hengboy.distributed.job.common.model.JobNode;
import com.gitee.hengboy.distributed.job.common.service.HeartSyncService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * 心跳检查runnable
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2019-01-08
 * Time：16:24
 * 个人博客：http://blog.yuqiyu.com
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
@AllArgsConstructor
@NoArgsConstructor
public class HeartSyncRunnable implements Runnable {
    /**
     * 心跳检查motan service
     */
    private HeartSyncService heartSyncService;

    @Override
    public void run() {
        JobNode jobClient = new JobNode();
        jobClient.setNodePort(12145);
        jobClient.setNodeAddress("127.0.0.1");
        heartSyncService.sync(jobClient);
    }
}
